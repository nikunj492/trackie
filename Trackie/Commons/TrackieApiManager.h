//
//  TrackieApiManager.h
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrackieObjectDelegate.h"

@interface TrackieApiManager : NSObject

//get shared instance of the api manager
+ (TrackieApiManager *) sharedApiManager;

//function to send get api call
- (void) getRequestWithParameters:(NSDictionary *)params withDelegate:(TrackieObject *)delegate;

@end
