//
//  TrackieLocationManager.m
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import "TrackieLocationManager.h"

#define LOCATION_VALID_TIME_INTERVAL 300  //seconds
#define MAXIMUM_LOCATION_ACCURACY_LIMIT 2000 //2 kms

@interface TrackieLocationManager() <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *currentLocation;

@end

@implementation TrackieLocationManager

+ (TrackieLocationManager *) sharedLocationManager {
    static TrackieLocationManager *_sharedLocationManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedLocationManager = [[TrackieLocationManager alloc] init];
    });
    
    return _sharedLocationManager;
}

- (id) init {
    self = [super init];
    if (self) {
        self.locationManager = [[CLLocationManager alloc] init];
        [self.locationManager setDelegate:self];
        [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    }
    
    return self;
}

+ (BOOL) isLocationPermissionAuthorized {
    return (([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) && [CLLocationManager locationServicesEnabled]);
}

- (void) refreshLocation {
    //if location permission is giving, simply ask location manager to update location
    if ([[self class] isLocationPermissionAuthorized]) {
        [self.locationManager startUpdatingLocation];
    }
    
    //if location services disabled globally
    else if (![CLLocationManager locationServicesEnabled]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOCATION_SERVICES_DISABLED object:nil];
    }
    
    //if user has denied the location permission to the app
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOCATION_SERVICES_DENIED object:nil];
    }
    
    //if app has not asked the user for the location permission
    //ask for permission and start updating location
    else {
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager startUpdatingLocation];
    }
}

#pragma mark - CLLocation Manager Delegate Methods
- (void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    //pass only if location returned is not cached and its accuracy is good
    if (![self cachedLocationReturned:newLocation] && newLocation.horizontalAccuracy < MAXIMUM_LOCATION_ACCURACY_LIMIT) {
        [manager stopUpdatingLocation];
        [self locationDataReceived:newLocation];
    }
}

- (void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusDenied && [CLLocationManager locationServicesEnabled]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOCATION_SERVICES_DENIED object:nil];
    }
}

- (void) locationDataReceived:(CLLocation *)location {
    self.currentLocation = location;
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOCATION_RECEIVED object:nil];
}

- (CLLocation *) getCurrentLocation {
    return self.currentLocation;
}

- (BOOL)cachedLocationReturned:(CLLocation *)location{
    NSDate *eventDate = location.timestamp; // get the timestamp of the coordinates
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow]; // get the difference between the location timestamp and current time
    if(howRecent < LOCATION_VALID_TIME_INTERVAL) //if the difference is less than LOCATION_VALID_TIME_INTERVAL then process else discard results
        return NO;
    return YES;
}

@end
