//
//  TrackieLocationManager.h
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

//notification fired once app has fetched the location
#define NOTIFICATION_LOCATION_RECEIVED          @"notification_location_received"

#define NOTIFICATION_LOCATION_SERVICES_DISABLED @"notification_location_disabled"
#define NOTIFICATION_LOCATION_SERVICES_DENIED   @"notification_location_denied"

@interface TrackieLocationManager : NSObject

//shared location manager
+ (TrackieLocationManager *) sharedLocationManager;

//call this to ask the app to fetch current location
- (void) refreshLocation;

//tells whether the user has authorized the app to give its location
+ (BOOL) isLocationPermissionAuthorized;

//call this once the app has fetched the current location to get location instance
- (CLLocation *) getCurrentLocation;

@end
