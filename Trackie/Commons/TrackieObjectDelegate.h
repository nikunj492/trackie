//
//  TrackieObjectDelegate.h
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TrackieObjectDelegate <NSObject>

//function to get the api path to which api call needs to be sent
@optional
- (NSString *) getAPIPathWithParams:(NSDictionary *)params;

//function called when api manager fetched the data successfully
@optional
- (void)didFetchWithInitialParams:(NSDictionary *)params;

//function called if api call gets failed
@optional
- (void) didFailWithError:(NSError *) error andInitialParams:(NSDictionary *)dictionary;

@end
