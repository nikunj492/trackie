//
//  CommonFunctions.h
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define X(v)                                            v.frame.origin.x
#define Y(v)                                            v.frame.origin.y
#define H(v)                                            v.frame.size.height
#define W(v)                                            v.frame.size.width
#define BOTTOM(v)                                       (v.frame.origin.y + v.frame.size.height)
#define AFTER(v)                                        (v.frame.origin.x + v.frame.size.width)
#define SIDE_PADDING                                    15

@interface CommonFunctions : NSObject

//get width of the phone
+ (CGFloat) getPhoneWidth;

//get height of the phone
+ (CGFloat) getPhoneHeight;

//function to parse number from api response
+ (NSNumber *) getNumberForKey:(NSString *)key fromDictionary:(NSDictionary *)dictionary withInitialValue:(NSNumber *)initialValue;

//function to parse string from api response
+ (NSString *) getStringForKey:(NSString *)key fromDictionary:(NSDictionary *)dictionary withInitialValue:(NSString *)initialValue;

@end
