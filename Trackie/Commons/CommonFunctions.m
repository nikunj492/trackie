//
//  CommonFunctions.m
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import "CommonFunctions.h"

@implementation CommonFunctions

+ (CGFloat) getPhoneWidth{
    return [UIScreen mainScreen].bounds.size.width;
}

+ (CGFloat) getPhoneHeight{
    return [UIScreen mainScreen].bounds.size.height;
}

+ (NSNumber *) getNumberForKey:(NSString *)key fromDictionary:(NSDictionary *)dictionary withInitialValue:(NSNumber *)initialValue {
    NSNumber *returnVal = initialValue;
    @try {
        returnVal = [dictionary valueForKeyPath:key];
    }
    @catch (NSException *exception) {
        NSLog(@"catched");
    }
    
    if (returnVal == nil){
        return initialValue;
    }
    else{
        if([returnVal isKindOfClass:[NSString class]]){
            NSString *tempString = (NSString *)returnVal;
            @try{
                returnVal = [NSDecimalNumber decimalNumberWithString:tempString];
            }
            @catch (NSException *exception){
                NSLog(@"catched in get number of key %@", returnVal);
            }
        }
    }
    
    if([returnVal isKindOfClass:[NSNumber class]]){
        return returnVal;
    }
    else{
        return initialValue;
    }
}

+ (NSString *) getStringForKey:(NSString *)key fromDictionary:(NSDictionary *)dictionary withInitialValue:(NSString *)initialValue {
    NSString *returnVal = initialValue;
    @try {
        returnVal = [dictionary valueForKeyPath:key];
    }
    @catch (NSException *exception) {
        NSLog(@"catched");
    }
    
    if (returnVal == nil){
        return initialValue;
    }
    else{
        returnVal = [NSString stringWithFormat:@"%@",returnVal];
    }
    
    if([returnVal isKindOfClass:[NSString class]]){
        return returnVal;
    }
    else{
        return initialValue;
    }
}

@end
