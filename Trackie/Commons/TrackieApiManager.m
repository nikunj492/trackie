//
//  TrackieApiManager.m
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import "TrackieApiManager.h"
#import <AFNetworking.h>

#define BASE_URL    @"http://api.511.org"
#define API_KEY     @"5013a647-63f4-4983-9567-822189107d96"

@interface TrackieApiManager()

@property (nonatomic, strong) AFHTTPSessionManager *apiManager;

@end

@implementation TrackieApiManager

+ (TrackieApiManager *)sharedApiManager {
    static TrackieApiManager *_sharedApiManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedApiManager = [[TrackieApiManager alloc] init];
    });
    
    return _sharedApiManager;
}

- (id) init {
    self = [super init];
    if (self) {
        self.apiManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        self.apiManager.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    return self;
}

//default params to send in every api call
- (NSDictionary *) getDefaultParams{
    return @{
        @"format":@"json",
        @"api_key":API_KEY
    };
}

- (void) getRequestWithParameters:(NSDictionary *)params withDelegate:(TrackieObject *)delegate {
    
    NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] initWithDictionary:params];
    [requestParams addEntriesFromDictionary:[self getDefaultParams]];
    
    NSString *apiEndPoint = @"";
    if ([delegate respondsToSelector:@selector(getAPIPathWithParams:)]) {
        apiEndPoint = [delegate getAPIPathWithParams:params];
    }
    
    NSString *apiPath = [NSString stringWithFormat:@"%@/%@", BASE_URL, apiEndPoint];
    apiPath = [self getAPIPath:apiPath WithQueryString:[self getQueryStringFromDictionary:requestParams]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:apiPath]];
    
    NSURLSessionDataTask *dataTask = [self.apiManager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            if ([delegate respondsToSelector:@selector(didFailWithError:andInitialParams:)]) {
                [delegate didFailWithError:error andInitialParams:requestParams];
            }
        } else {
            if ([delegate respondsToSelector:@selector(didFetchWithInitialParams:)]) {
                //parsing response in background thread and then coming to main thread to call the delegate callback
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    [delegate parseObject:responseObject withInitialParams:requestParams];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [delegate didFetchWithInitialParams:requestParams];
                    });
                });
            }
        }
    }];
    
    [dataTask resume];
}

- (NSString *) getAPIPath:(NSString *)apiPath WithQueryString:(NSString *)queryString{
    if([apiPath rangeOfString:@"?"].location == NSNotFound) {
        apiPath = [apiPath stringByAppendingFormat:@"?%@", queryString];
    }
    else {
        apiPath = [apiPath stringByAppendingFormat:@"&%@", queryString];
    }
    return apiPath;
}

- (NSString *) getQueryStringFromDictionary:(NSDictionary *)dictionary {
    NSMutableArray *parts = [NSMutableArray array];
    for (id key in dictionary) {
        id value = [dictionary objectForKey: key];
        NSString *valueString = [NSString stringWithFormat:@"%@", value];
        valueString = [valueString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
        NSString *part = [NSString stringWithFormat: @"%@=%@", key, valueString];
        [parts addObject: part];
    }
    
    return [parts componentsJoinedByString: @"&"];
}

@end
