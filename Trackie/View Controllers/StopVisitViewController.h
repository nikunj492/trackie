//
//  StopVisitViewController.h
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StopVisitViewController : UIViewController

- (id) initWithStopId:(NSString *)stopId;

@end
