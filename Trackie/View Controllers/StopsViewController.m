//
//  ViewController.m
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import "StopsViewController.h"
#import "FetchAllStops.h"
#import "StopsTableViewCell.h"
#import "TrackieLocationManager.h"
#import "StopVisitViewController.h"

#define STOPS_CELL_REUSE_IDENTIFIER @"stops_cell"

@interface StopsViewController () <FetchAllStopsDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UILabel *detectingLabel;
@property (nonatomic, strong) UITableView *stopsTableView;
@property (nonatomic, strong) FetchAllStops *fetchAllStopsObject;

@end

@implementation StopsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self listenToNotifications];
    [self setHeader];
    [self createViews];
    
    //start location detection
    [[TrackieLocationManager sharedLocationManager] refreshLocation];
}

- (void) listenToNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationReceived) name:NOTIFICATION_LOCATION_RECEIVED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationDenied) name:NOTIFICATION_LOCATION_SERVICES_DENIED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationServicesDisabled) name:NOTIFICATION_LOCATION_SERVICES_DISABLED object:nil];
}

- (void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self.detectingLabel setFrame:self.view.bounds];
    [self.stopsTableView setFrame:self.view.bounds];
}


#pragma mark - Views Function
- (void) setHeader {
    [self setTitle:@"Nearest Stops"];
}

- (void) createViews {
    [self createDetectingLabel];
    [self createStopsTableView];
}

- (void) createDetectingLabel {
    self.detectingLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self.detectingLabel setText:@"Detecting Location ..."];
    [self.detectingLabel setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:self.detectingLabel];
}

- (void) createStopsTableView {
    self.stopsTableView = [[UITableView alloc] initWithFrame:CGRectZero];
    [self.stopsTableView setDelegate:self];
    [self.stopsTableView setDataSource:self];
    [self.stopsTableView setBackgroundColor:[UIColor whiteColor]];
    [self.stopsTableView setHidden:true];
    [self.view addSubview:self.stopsTableView];
}

#pragma mark - Table View Delegate Methods
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fetchAllStopsObject.busStopsArray.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [StopsTableViewCell getHeight];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    StopsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:STOPS_CELL_REUSE_IDENTIFIER];
    if (cell == nil) {
        cell = [[StopsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:STOPS_CELL_REUSE_IDENTIFIER];
    }
    
    BusStop *busStop = [self.fetchAllStopsObject.busStopsArray objectAtIndex:indexPath.row];
    [cell setBusStop:busStop];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    BusStop *busStop = [self.fetchAllStopsObject.busStopsArray objectAtIndex:indexPath.row];
    StopVisitViewController *stopVisitVC = [[StopVisitViewController alloc] initWithStopId:busStop.stopId];
    [self.navigationController pushViewController:stopVisitVC animated:true];
}

#pragma mark - Location Handling
- (void) locationReceived {
    [self getData];
}

- (void) locationDenied {
    [self.detectingLabel setText:@"Location Permission Denied"];
}

- (void) locationServicesDisabled {
    [self.detectingLabel setText:@"Location Services Disabled"];
}

- (void) getData {
    [self.detectingLabel setText:@"Fetching Data ..."];
    self.fetchAllStopsObject = [[FetchAllStops alloc] init];
    [self.fetchAllStopsObject fetchAllStopsWithDelegate:self];
}

#pragma mark - Fetch All Stops delegate methods
- (void) allStopsFetchedSuccessfully {
    [self.detectingLabel setHidden:true];
    [self.stopsTableView reloadData];
    [self.stopsTableView setHidden:false];
}

- (void) allStopsFetchingFailedWithError:(NSError *)error {
    [self.detectingLabel setText:@"Fetching Failed"];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
