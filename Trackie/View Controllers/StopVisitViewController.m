//
//  ViewController.m
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import "StopVisitViewController.h"
#import "FetchMonitoredStopVisits.h"
#import "StopVisitTableViewCell.h"
#import "TrackieLocationManager.h"

#define STOP_VISIT_CELL_REUSE_IDENTIFIER @"stop_visit_cell"

@interface StopVisitViewController () <FetchMonitoredStopVisitsDelegate, UITableViewDelegate, UITableViewDataSource> {
    BOOL dataFetched;
}

@property (nonatomic, strong) NSString *stopId;
@property (nonatomic, strong) UILabel *loadingLabel;
@property (nonatomic, strong) UITableView *stopsVisitTableView;
@property (nonatomic, strong) FetchMonitoredStopVisits *fetchMonitoredStopsVisitObject;

@end

@implementation StopVisitViewController

- (id) initWithStopId:(NSString *)stopId {
    self = [super init];
    if (self) {
        self.stopId = stopId;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self getData];
    [self setHeader];
    [self createViews];
}

- (void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self.loadingLabel setFrame:self.view.bounds];
    [self.stopsVisitTableView setFrame:self.view.bounds];
}

#pragma mark - Views Function
- (void) setHeader {
    [self setTitle:@"Stop Visits"];
}

- (void) createViews {
    [self createLoadingLabel];
    [self createStopsVisitTableView];
}

- (void) createLoadingLabel {
    self.loadingLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self.loadingLabel setText:@"Fetching Data ..."];
    [self.loadingLabel setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:self.loadingLabel];
}

- (void) createStopsVisitTableView {
    self.stopsVisitTableView = [[UITableView alloc] initWithFrame:CGRectZero];
    [self.stopsVisitTableView setDelegate:self];
    [self.stopsVisitTableView setDataSource:self];
    [self.stopsVisitTableView setBackgroundColor:[UIColor whiteColor]];
    [self.stopsVisitTableView setHidden:true];
    [self.view addSubview:self.stopsVisitTableView];
}

#pragma mark - Table View Delegate Methods
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fetchMonitoredStopsVisitObject.monitoredStopVisitsArray.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [StopVisitTableViewCell getHeight];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    StopVisitTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:STOP_VISIT_CELL_REUSE_IDENTIFIER];
    if (cell == nil) {
        cell = [[StopVisitTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:STOP_VISIT_CELL_REUSE_IDENTIFIER];
    }
    
    MonitoredStopVisit *stopVisit = [self.fetchMonitoredStopsVisitObject.monitoredStopVisitsArray objectAtIndex:indexPath.row];
    [cell setStopVisit:stopVisit];
    
    return cell;
}

#pragma mark - Location Handling
- (void) getData {
    self.fetchMonitoredStopsVisitObject = [[FetchMonitoredStopVisits alloc] init];
    [self.fetchMonitoredStopsVisitObject fetchAllMonitoredStopVisitsWithDelegate:self withStopId:self.stopId];
}

#pragma mark - Fetch All Stops delegate methods
- (void) allMonitoredStopVisitsFetchedSuccessfully {
    if (self.fetchMonitoredStopsVisitObject.monitoredStopVisitsArray.count == 0) {
        [self.loadingLabel setText:@"No Upcoming Routes Found"];
    } else {
        [self.loadingLabel setHidden:true];
        [self.stopsVisitTableView reloadData];
        [self.stopsVisitTableView setHidden:false];
    }
}

- (void) allMonitoredStopVisitsFetchingFailedWithError:(NSError *)error {
    [self.loadingLabel setText:@"Fetching Failed"];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
