//
//  StopsTableViewCell.h
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MonitoredStopVisit.h"

@interface StopVisitTableViewCell : UITableViewCell

- (void) setStopVisit:(MonitoredStopVisit *) stopVisit;
+ (CGFloat) getHeight;

@end
