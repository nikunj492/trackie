//
//  StopsTableViewCell.m
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import "StopVisitTableViewCell.h"

#define TOP_PADDING     10
#define BOTTOM_PADDING  10
#define LABEL_HEIGHT    20

@interface StopVisitTableViewCell()

@property (nonatomic, strong) UILabel *lineNameLabel;
@property (nonatomic, strong) UILabel *arrivalTimeLabel;
@property (nonatomic, strong) UILabel *departureTimeLabel;

@end

@implementation StopVisitTableViewCell

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self createViews];
    }
    
    return self;
}

- (void) createViews {
    [self createLineNameLabel];
    [self createArrivalTimeLabel];
    [self createDepartureTimeLabel];
}

- (void) createLineNameLabel {
    self.lineNameLabel = [[UILabel alloc] init];
    [self.contentView addSubview:self.lineNameLabel];
}

- (void) createArrivalTimeLabel {
    self.arrivalTimeLabel = [[UILabel alloc] init];
    [self.contentView addSubview:self.arrivalTimeLabel];
}

- (void) createDepartureTimeLabel {
    self.departureTimeLabel = [[UILabel alloc] init];
    [self.contentView addSubview:self.departureTimeLabel];
}

- (void) setStopVisit:(MonitoredStopVisit *)stopVisit {
    [self.lineNameLabel setText:[NSString stringWithFormat:@"Line Name: %@", stopVisit.lineName]];
    [self.arrivalTimeLabel setText:[NSString stringWithFormat:@"Arrival Time: %@", stopVisit.arrivalTime]];
    [self.departureTimeLabel setText:[NSString stringWithFormat:@"Departure Time: %@", stopVisit.departureTime]];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    [self.lineNameLabel setFrame:CGRectMake(SIDE_PADDING, TOP_PADDING, W(self) - 2*SIDE_PADDING, LABEL_HEIGHT)];
    [self.arrivalTimeLabel setFrame:CGRectMake(X(self.lineNameLabel), BOTTOM(self.lineNameLabel), W(self.lineNameLabel), H(self.lineNameLabel))];
    [self.departureTimeLabel setFrame:CGRectMake(X(self.arrivalTimeLabel), BOTTOM(self.arrivalTimeLabel), W(self.arrivalTimeLabel), H(self.arrivalTimeLabel))];
}

+ (CGFloat) getHeight {
    return TOP_PADDING + 3 * LABEL_HEIGHT + BOTTOM_PADDING;
}

@end
