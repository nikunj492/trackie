//
//  StopsTableViewCell.h
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusStop.h"

@interface StopsTableViewCell : UITableViewCell

- (void) setBusStop:(BusStop *) busStop;
+ (CGFloat) getHeight;

@end
