//
//  StopsTableViewCell.m
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import "StopsTableViewCell.h"

#define TOP_PADDING     10
#define BOTTOM_PADDING  10
#define LABEL_HEIGHT    20

@interface StopsTableViewCell()

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *distanceLabel;

@end

@implementation StopsTableViewCell

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self createViews];
    }
    
    return self;
}

- (void) createViews {
    [self createNameLabel];
    [self createDistanceLabel];
}

- (void) createNameLabel {
    self.nameLabel = [[UILabel alloc] init];
    [self.contentView addSubview:self.nameLabel];
}

- (void) createDistanceLabel {
    self.distanceLabel = [[UILabel alloc] init];
    [self.contentView addSubview:self.distanceLabel];
}

- (void) setBusStop:(BusStop *) busStop {
    [self.nameLabel setText:busStop.name];
    [self.distanceLabel setText:[NSString stringWithFormat:@"Distance: %@ m", busStop.distance]];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    [self.nameLabel setFrame:CGRectMake(SIDE_PADDING, TOP_PADDING, W(self) - 2*SIDE_PADDING, LABEL_HEIGHT)];
    [self.distanceLabel setFrame:CGRectMake(X(self.nameLabel), BOTTOM(self.nameLabel), W(self.nameLabel), H(self.nameLabel))];
}

+ (CGFloat) getHeight {
    return TOP_PADDING + 2 * LABEL_HEIGHT + BOTTOM_PADDING;
}

@end
