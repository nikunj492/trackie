//
//  FetchAllStops.h
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BusStop.h"

@protocol FetchAllStopsDelegate <NSObject>

@required
- (void) allStopsFetchedSuccessfully;

@required
- (void) allStopsFetchingFailedWithError:(NSError *)error;

@end

@interface FetchAllStops : TrackieObject

@property (nonatomic, weak) id<FetchAllStopsDelegate> delegate;
@property (nonatomic, strong) NSArray *busStopsArray;

- (void) fetchAllStopsWithDelegate:(id<FetchAllStopsDelegate>) delegate;

@end
