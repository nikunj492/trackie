//
//  TrackieObject.h
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrackieObjectDelegate.h"

/*
basic model object
every model object should be subclass of this. 
It implements the TrackieObjectDelegate
*/

@interface TrackieObject : NSObject <TrackieObjectDelegate>

//function called by the api manager once it has fetched data so that respective model can parse it
- (void) parseObject:(NSDictionary *)responseObject withInitialParams:(NSDictionary *)params;

@end
