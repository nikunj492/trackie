//
//  BusStop.m
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import "BusStop.h"

@implementation BusStop

- (void) parseObject:(NSDictionary *)responseObject withInitialParams:(NSDictionary *)params {
    self.stopId = [CommonFunctions getStringForKey:@"id" fromDictionary:responseObject withInitialValue:self.stopId];
    self.name = [CommonFunctions getStringForKey:@"Name" fromDictionary:responseObject withInitialValue:self.name];
    self.stopType = [CommonFunctions getStringForKey:@"StopType" fromDictionary:responseObject withInitialValue:self.stopType];
    
    NSNumber *latitude = [CommonFunctions getNumberForKey:@"Location.Latitude" fromDictionary:responseObject withInitialValue:nil];
    NSNumber *longitude = [CommonFunctions getNumberForKey:@"Location.Longitude" fromDictionary:responseObject withInitialValue:nil];

    self.location = [[CLLocation alloc] initWithLatitude:latitude.doubleValue longitude:longitude.doubleValue];
}

- (void) setDistanceWith:(CLLocation *)currentLocation {
    self.distance = @([[NSString stringWithFormat:@"%.2f", [self.location distanceFromLocation:currentLocation]] floatValue]);
}

@end
