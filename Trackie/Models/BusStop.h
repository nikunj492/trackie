//
//  BusStop.h
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface BusStop : TrackieObject

@property (nonatomic, strong) NSString *stopId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *stopType;
@property (nonatomic, strong) NSNumber *distance;
@property (nonatomic, strong) CLLocation *location;

- (void) setDistanceWith:(CLLocation *) currentLocation;

@end
