//
//  FetchAllStops.m
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import "FetchAllStops.h"
#import "TrackieApiManager.h"
#import "TrackieLocationManager.h"

@interface FetchAllStops() {
    BOOL isLoading;
}

@end

@implementation FetchAllStops

- (void) fetchAllStopsWithDelegate:(id<FetchAllStopsDelegate>) delegate {
    if (isLoading) {
        return;
    }
    
    isLoading = true;
    self.delegate = delegate;
    [[TrackieApiManager sharedApiManager] getRequestWithParameters:@{@"operator_id":@"AC Transit"} withDelegate:self];
}

- (void) parseObject:(NSDictionary *)responseObject withInitialParams:(NSDictionary *)params {
    CLLocation *currentLocation = [[TrackieLocationManager sharedLocationManager] getCurrentLocation];

    NSMutableArray *tempBusStopsArray = [[NSMutableArray alloc] init];
    for (NSDictionary *busStopDict in [responseObject valueForKeyPath:@"Contents.dataObjects.ScheduledStopPoint"]) {
        BusStop *busStopObject = [[BusStop alloc] init];
        [busStopObject parseObject:busStopDict withInitialParams:params];
        
        if ([busStopObject.stopType isEqualToString:@"onstreetBus"]) {
            [busStopObject setDistanceWith:currentLocation];
            [tempBusStopsArray addObject:busStopObject];
        }
    }
    
    //sorting busstops array using sorting by distance
    self.busStopsArray = [tempBusStopsArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSNumber *first = [(BusStop *)a distance];
        NSNumber *second = [(BusStop *)b distance];
        return [first compare:second];
    }];
    
    //keeping nearest 20 busstops only
    self.busStopsArray = [self.busStopsArray subarrayWithRange:NSMakeRange(0, 20)];
}

- (NSString *) getAPIPathWithParams:(NSDictionary *)params {
    return @"transit/stops";
}

- (void) didFetchWithInitialParams:(NSDictionary *)params {
    isLoading = false;
    if ([self.delegate respondsToSelector:@selector(allStopsFetchedSuccessfully)]) {
        [self.delegate allStopsFetchedSuccessfully];
    }
}

- (void) didFailWithError:(NSError *)error andInitialParams:(NSDictionary *)dictionary {
    isLoading = false;
    if ([self.delegate respondsToSelector:@selector(allStopsFetchingFailedWithError:)]) {
        [self.delegate allStopsFetchingFailedWithError:error];
    }
}

@end
