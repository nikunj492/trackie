//
//  FetchAllStops.m
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import "FetchMonitoredStopVisits.h"
#import "TrackieApiManager.h"
#import "TrackieLocationManager.h"

@implementation FetchMonitoredStopVisits

- (void) fetchAllMonitoredStopVisitsWithDelegate:(id<FetchMonitoredStopVisitsDelegate>)delegate withStopId:(NSString *)stopId {
    self.delegate = delegate;
    [[TrackieApiManager sharedApiManager] getRequestWithParameters:@{@"agency":@"actransit", @"stopCode":stopId} withDelegate:self];
}

- (void) parseObject:(NSDictionary *)responseObject withInitialParams:(NSDictionary *)params {
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (NSDictionary *busStopDict in [responseObject valueForKeyPath:@"ServiceDelivery.StopMonitoringDelivery.MonitoredStopVisit"]) {
        MonitoredStopVisit *stopVisit = [[MonitoredStopVisit alloc] init];
        [stopVisit parseObject:busStopDict withInitialParams:params];
        [tempArray addObject:stopVisit];
    }
    
    self.monitoredStopVisitsArray = [NSArray arrayWithArray:tempArray];
}

- (NSString *) getAPIPathWithParams:(NSDictionary *)params {
    return @"transit/StopMonitoring";
}

- (void) didFetchWithInitialParams:(NSDictionary *)params {
    if ([self.delegate respondsToSelector:@selector(allMonitoredStopVisitsFetchedSuccessfully)]) {
        [self.delegate allMonitoredStopVisitsFetchedSuccessfully];
    }
}

- (void) didFailWithError:(NSError *)error andInitialParams:(NSDictionary *)dictionary {
    if ([self.delegate respondsToSelector:@selector(allMonitoredStopVisitsFetchingFailedWithError:)]) {
        [self.delegate allMonitoredStopVisitsFetchingFailedWithError:error];
    }
}

@end
