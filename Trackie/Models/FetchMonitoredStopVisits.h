//
//  FetchMonitoredStopVisits.h
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import "TrackieObject.h"
#import "MonitoredStopVisit.h"

@protocol FetchMonitoredStopVisitsDelegate <NSObject>

@required
- (void) allMonitoredStopVisitsFetchedSuccessfully;

@required
- (void) allMonitoredStopVisitsFetchingFailedWithError:(NSError *)error;

@end

@interface FetchMonitoredStopVisits : TrackieObject

@property (nonatomic, weak) id<FetchMonitoredStopVisitsDelegate> delegate;
@property (nonatomic, strong) NSArray *monitoredStopVisitsArray;

- (void) fetchAllMonitoredStopVisitsWithDelegate:(id<FetchMonitoredStopVisitsDelegate>) delegate withStopId:(NSString *)stopId;

@end
