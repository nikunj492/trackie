//
//  MonitoredStopVisit.m
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import "MonitoredStopVisit.h"

@implementation MonitoredStopVisit

- (void) parseObject:(NSDictionary *)responseObject withInitialParams:(NSDictionary *)params {
    self.lineName = [CommonFunctions getStringForKey:@"MonitoredVehicleJourney.PublishedLineName" fromDictionary:responseObject withInitialValue:self.lineName];
    self.arrivalTime = [CommonFunctions getStringForKey:@"MonitoredVehicleJourney.MonitoredCall.AimedArrivalTime" fromDictionary:responseObject withInitialValue:self.arrivalTime];
    self.departureTime = [CommonFunctions getStringForKey:@"MonitoredVehicleJourney.MonitoredCall.AimedDepartureTime" fromDictionary:responseObject withInitialValue:self.departureTime];
}

@end
