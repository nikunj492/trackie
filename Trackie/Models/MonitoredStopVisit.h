//
//  MonitoredStopVisit.h
//  Trackie
//
//  Created by Nikunj on 19/11/16.
//  Copyright © 2016 Nikunj. All rights reserved.
//

#import "TrackieObject.h"

@interface MonitoredStopVisit : TrackieObject

@property (nonatomic, strong) NSString *lineName;
@property (nonatomic, strong) NSString *arrivalTime;
@property (nonatomic, strong) NSString *departureTime;

@end
