# Trackie #

It is an iOS application which takes your location and shows the nearest bus stops to you. Then for each bus stop, you can check the upcoming buses arrival and departure time in realtime. All of the code is written by me except one external library which I am using.

### API
I am using 511 SF Bay Api. I can find two apis which are helpful to me. One is to fetch all bus stops powered by a bus agency. Right now, the app is running for only bus agency i.e. AC Transit. We can make the control in app also to change the bus agency if given time. I am fetching all the bus stops and then showing only nearest 20 bus stops to user. The second api I am using is realtime bus stop monitoring. Given a bus stop and agency, it fetches the upcoming buses with their arrival and departure time.

### External Libraries
I am using AFNetworking as a networking library. It is added via the Pods.

### Code Structure
All of the code is divided under the following sections:

####Commons
1. CommonFunctions: It includes basic functions used by all
2. TrackieApiManager: It is a wrapper written over Afnetworking manager. It is the central class used to send all api calls.
3. TrackieLocationManager: It is the central class to handle geo data provided by the os. Anybody requiring geolocation needs to call refreshlocation function.

####Models
1. TrackieObject: It is the superclass for all the models. It implements TrackieObjectDelegate which has the functions for TrackieApiManager callback. It also implements parseObject function to parse the data coming from the api.
2. BusStop: It represents a bus stop (name, latitude, longitude, id etc.)
3. MonitoredStopVisit: It represents a realtime visit of bus (arrivaltime, departuretime, line number)
4. FetchAllStops: It asks the apimanager to send the api call and then parse all busstops.
5. FetchMonitoredStopVisits: It does the same thing as fetchallstops for monitoredstopvisits.

####Views
1. StopsTableViewCell: It is a tableviewcell which shows the busstop name and distance from current location.
2. StopVisitTableViewCell: It is a tableviewcell which shows the arrival time, departure time and line number of an upcoming bus at busstop.

####View Controllers
1. StopsViewController: It is the root view controller of the app. It contains a tableview used to show the busstops nearby. It first asks the locationmanager of the current location of the person and then uses this locaiton to send the apicalls and calculate the nearby busstops. It then shows the same data in the tableview.

2. StopVisitViewController: Given a stop id, it fetches the upcoming buses on that stop and shows them in a tableview.

### Screenshots
First Page
![IMG_3198.PNG](https://bitbucket.org/repo/yEAde9/images/3518046666-IMG_3198.PNG)

Second Page
![IMG_3199.PNG](https://bitbucket.org/repo/yEAde9/images/532331131-IMG_3199.PNG)

###Future Work
* We can explore other good apis which get the data directly. Ideally we want the nearest busstops which upcoming bus in the first page only. But this api doesn't provide this data in one api call. That's why I have made the second page to get the upcoming buses.
* We can also show all bus stops on a google map and then tapping on each bus stop, we can show the upcoming bus.
* We can also add the refresh button on each view controller to fetch the latest data or in case fetching data gets failed.
* We can also use the line api independently to show the line name instead of the line number.
* We can also show the time in minutes left.